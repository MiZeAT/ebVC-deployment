# Excel based vCenter deployment

Deployment of "vCenter Server Appliance with an embedded Platform Services Controller" on an ESXi host

## Table of Contents


* [changelog](#changelog)
* [prerequisites](#prerequisites)
* [Use Import-Excel to get data](#Import-Excel)
* [Use vcsa-deploy.exe to deploy OVF-file](#vcsa-deploy)
* [VC_data](#VC_data)

## changelog

### **2018-08-01**
*Add ISO selector

### **2018-07-31**
*Start



## prerequisites

* no installed excel required, but
* ImportExcel Module
* VMware.PowerCLI Module


## Import-Excel

Use Import-Excel to get user data

## vcsa-deploy

Use vcsa-cli-installer/win32/vcsa-deploy.exe to instal VCSA


## VC_data

Fill VC_data.xlsx with data