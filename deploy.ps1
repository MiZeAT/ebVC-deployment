##Excel based VC Deployment

$LogFile="./logfile.log"
$VCSAisoPath="C:\path\to\ISOfile\VMware-VCSA-all.iso"


#logger Function
Function logger {
    param(
    [Parameter(Mandatory=$true)]
    [String]$message
    )

    $timeStamp = Get-Date -Format "yyyy-MM-dd_HH:mm:ss"

    Write-Host -NoNewline -ForegroundColor White "[$timestamp]"
    Write-Host -ForegroundColor Green " $message"
    $logMessage = "[$timeStamp] $message"
    $logMessage | Out-File -Append -FilePath $LogFile
}


#
logger "check if module 'VMware.PowerCLI' is installed ..."
if (!(Get-Module -ListAvailable VMware.PowerCLI))
{logger "VMware.PowerCLI not installed" Write-Host "Please install ImportExcel Module from PSGallery
 
 e.g. " -NoNewline
 Write-Host -ForegroundColor Magenta " Find-Module VMware.PowerCLI |Install-Module"
 }

 
#
logger "check if module 'ImportExcel' is installed ..."
if (!(Get-Module -ListAvailable ImportExcel))
 {logger "ImportExcel not installed"  Write-Host "Please install ImportExcel Module from https://github.com/dfinke/ImportExcel
  or PSGallery
  
  e.g. " -NoNewline
  Write-Host -ForegroundColor Magenta " Find-Module ImportExcel |Install-Module"
 }

 
# 
logger "import user data from Excel file ..."
$xlsdata=Import-Excel -Path .\VC_data.xlsx -WorksheetName "VC_data"


#
logger "check if VMware vCenter ISO File is defined/selected ..."
if ($VCSAisoPath -EQ "C:\path\to\ISOfile\VMware-VCSA-all.iso")
{
    Write-Host "Select VMware vCenter ISO File to deploy"
	[System.Reflection.Assembly]::LoadWithPartialName("System.windows.forms") | Out-Null
    $OpenFileDialog = New-Object System.Windows.Forms.OpenFileDialog
    $OpenFileDialog.initialDirectory = [System.IO.Directory]::GetCurrentDirectory()
    $openFileDialog.title = "Select VMware vCenter ISO File to deploy"
    $OpenFileDialog.filter = "ISO (*.iso)| *.iso"
    $OpenFileDialog.ShowDialog() #| Out-Null	
    $VCSAisoPath=$OpenFileDialog.filename
}
logger "selected ISO file: $VCSAisoPath ..."
 
 
#
logger "mount ISO '$VCSAisoPath' ..."
Mount-DiskImage -StorageTyp ISO -ImagePath $VCSAisoPath -Access ReadOnly -Verbose
$myvol=Get-DiskImage $VCSAisoPath |Get-Volume 
$mydrive=$myvol.DriveLetter


#
logger "get VCSA Version ..."
$mypath=$mydrive+":/vcsa/version.txt"
$myVCSAversion=Get-Content -Raw -Path $mypath
$myVCSAversion| Out-File -Append -FilePath $LogFile
Write-Host "Install $myVCSAversion"


#
logger "generate config json ..."
$myconfigpath=$mydrive+":/vcsa-cli-installer/templates/install/embedded_vCSA_on_ESXi.json"
$config = (Get-Content -Raw -LiteralPath $myconfigpath) | convertfrom-json
$config.new_vcsa.esxi.hostname = "$($xlsdata.VIServer)"
$config.new_vcsa.esxi.username = "$($xlsdata.VIUsername)"
$config.new_vcsa.esxi.password = "$($xlsdata.VIPassword)"
$config.new_vcsa.esxi.deployment_network = "$($xlsdata.VMNetwork)"
$config.new_vcsa.esxi.datastore = "$($xlsdata.VIDatastore)"
$config.new_vcsa.appliance.thin_disk_mode = $true
$config.new_vcsa.appliance.deployment_option = "$($xlsdata.VCSADeploymentSize)"
$config.new_vcsa.appliance.name = "$($xlsdata.VCSADisplayName)"
$config.new_vcsa.network.ip_family = "ipv4"
$config.new_vcsa.network.mode = "static"
$config.new_vcsa.network.ip = "$($xlsdata.VCSAIPAddress)"
$config.new_vcsa.network.dns_servers[0] = "$($xlsdata.VMDNS)"
$config.new_vcsa.network.prefix = "$($xlsdata.VCSAPrefix)"
$config.new_vcsa.network.gateway = "$($xlsdata.VMGateway)"
$config.new_vcsa.network.system_name = "$($xlsdata.VCSAHostname)"
$config.new_vcsa.os.password = "$($xlsdata.VCSARootPassword)"
if($($xlsdata.VCSASSHEnableVar) -eq "true") {
    $VCSASSHEnableVar = $true
} else {
    $VCSASSHEnableVar = $false
}
$config.new_vcsa.os.ntp_servers = "$($xlsdata.VMNTP)"
$config.new_vcsa.os.ssh_enable = $VCSASSHEnableVar
$config.new_vcsa.sso.password = "$($xlsdata.VCSASSOPassword)"
$config.new_vcsa.sso.domain_name = "$($xlsdata.VCSASSODomainName)"


#
logger "Creating VCSA JSON Configuration file for deployment ..."
$config | ConvertTo-Json | Set-Content -Path "$($ENV:Temp)\myjsontemplate.json"


# 
logger "Deploy using the VCSA CLI Installer ..."
$myexepath=$mydrive+":/vcsa-cli-installer/win32/vcsa-deploy.exe"
Invoke-Expression "$myexepath install --verbose --no-ssl-certificate-verification --accept-eula --acknowledge-ceip $($ENV:Temp)\myjsontemplate.json"| Out-File -Append -FilePath $LogFile


#
logger "unmount ISO '$VCSAisoPath' ..."
Dismount-DiskImage -ImagePath $VCSAisoPath


#
logger "done ..."